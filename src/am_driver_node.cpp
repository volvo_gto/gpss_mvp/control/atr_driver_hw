#include <memory>
#include <chrono>
#include <functional>
#include <string>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "atr_driver/am_driver_node.h"
#include <boost/shared_ptr.hpp>

#include <chrono>
#include <iostream>
//#include "atr_interfaces/msg/OdomMsg.hpp"  
#include <typeinfo>

#include <ctime>

using std::placeholders::_1;
using namespace std::chrono_literals;
using namespace std::chrono;
using namespace std;


  Gpss::ATRDriver::ATRDriver(Husqvarna::AutomowerSafePtr am_t)
  : Node("ATR_driver")
  {
    std::vector<std::string> param_names = { "kp", "ki", "kd"};
    for (auto&& i : param_names){
      declare_parameter(i);
    }
    std::vector<rclcpp::Parameter> params = this->get_parameters(param_names);
    //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"pid param: "+params.at(1).as_string());

    am = am_t;
    am->setup();
    am->initAutomowerBoard(double(100), params.at(0).as_double(), params.at(1).as_double(), params.at(2).as_double());
    
    new_ref = false;
    subscription_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10, std::bind(&ATRDriver::velocity_callback, this, _1));
    subscription_new_ref = this->create_subscription<std_msgs::msg::Bool>("new_ref", 10, std::bind(&ATRDriver::new_ref_callback, this, _1));

    //publisher_ = this->create_publisher<geometry_msgs::msg::Pose2D>("odom", 10);
    publisher_ = this->create_publisher<atr_interfaces::msg::Pose2DStamped>("odom", 10);
    timer_pub = this->create_wall_timer(10ms, std::bind(&ATRDriver::odometry_callback, this));
    
    publisher_ctrl_data = this->create_publisher<msg_pkg::msg::ControlSignals>("ctrl_data", 10);
    
    //auto Gpss::ATRDriver::request = std::make_shared<msg_pkg::srv::Collision::Request>();

    //Husqvarna::AutomowerSafePtr am(new Husqvarna::AutomowerSafe);
    
  }

  void Gpss::ATRDriver::new_ref_callback(const std_msgs::msg::Bool::SharedPtr b)
  {
    
    this->new_ref = true;
    
  }

  void Gpss::ATRDriver::velocity_callback(const geometry_msgs::msg::Twist::SharedPtr vel)
  {
    am->getWheelData();
    this->lin_vel = (double)vel->linear.x;
    this->ang_vel = (double)vel->angular.z;

    if ((am->sensorStatus.sensor_status & 0x0004) || this->new_ref==false ) 
    {
      am->sendWheelPower(0.0, 0.0);
      RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Collision - Node - stop wheels");

      this->new_ref=false;
      
      
    }
    else
    {
      //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"No Collision - Node- velocity callback");
      am->wanted_lv = lin_vel - ang_vel * am->AUTMOWER_WHEEL_BASE_WIDTH / 2;
      am->wanted_rv = lin_vel + ang_vel * am->AUTMOWER_WHEEL_BASE_WIDTH / 2;

      if ((am->wanted_rv < 0.025) && (am->wanted_lv < 0.025) && (am->wanted_rv > -0.025) && (am->wanted_lv > -0.025)) {
        am->wanted_lv = 0;
        am->wanted_rv = 0;
      }


      am->regulateVelocity();
    }

    // publish controller signal
    this->message_ctrl_data.power_l = am->power_l;
    this->message_ctrl_data.power_r = am->power_r;
    this->message_ctrl_data.wanted_lv = am->wanted_lv;
    this->message_ctrl_data.wanted_rv = am->wanted_rv;
    this->message_ctrl_data.current_lv = am->current_lv;
    this->message_ctrl_data.current_rv = am->current_rv;
    this->message_ctrl_data.ref_lin_vel = this->lin_vel;
    this->message_ctrl_data.ref_ang_vel = this->ang_vel;

    publisher_ctrl_data->publish(message_ctrl_data);

    

    //RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
  }
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscription_;
  rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr subscription_new_ref;
  //rclcpp::Client<msg_pkg::srv::Collision>::SharedPtr client_;


  void Gpss::ATRDriver::odometry_callback()
  {
  
    am->getSensorStatus();
    /*
    if (am->sensorStatus.sensor_status & 0x0004)
    {
      RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Collision - Node");
    }
    else
    {
      RCLCPP_INFO(rclcpp::get_logger("my_logger"),"No collision - Node");
    }
    */
    am->getEncoderData();
    am->updateOdometry(double(100));

    
    this->message.pose.x = am->xdist;
    this->message.pose.y = am->ydist;
    this->message.pose.theta = am->delta_yaw;
    //this->message.timestamp = (_Float32) std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    //milliseconds timestamp_test1 = duration_cast< milliseconds >(system_clock::now().time_since_epoch());
    this->message.timestamp = (double) duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();


    //RCLCPP_INFO(this->get_logger(), "Publishing ODOM TIMESTAMP: '%f'", this->message.timestamp);
    //RCLCPP_INFO(this->get_logger(), "Timestamp test 1: '%f'", timestamp_test1);
    //RCLCPP_INFO(this->get_logger(), "Timestamp test 2 double: '%f'", tp_2);

    //long double sysTime = time(0);
    //long double sysTimeMS = sysTime*1000;

    //RCLCPP_INFO(this->get_logger(), "Test3: '%f' ", sysTimeMS );

    //time_t now = std::time(0);
    //std::string sysTimeMS2 = std::ctime(&now);

    //RCLCPP_INFO(this->get_logger(), "Test4:  '%f' ", sysTimeMS2 );



    //RCLCPP_INFO(this->get_logger(), "Am_driver TIMESTAMP");
    // RCLCPP_INFO(this->get_logger(), "Publishing ODOM TIMESTAMP TYPE: '%s'", typeid(this->message.timestamp).name());
    // RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Publishing ODOM TIMESTAMP: '%s'", this->message.timestamp);
    // RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Automower::STRANGE distance? - zeroint counters from mower");
    publisher_->publish(message);

  }
  rclcpp::TimerBase::SharedPtr timer_pub;
  //rclcpp::Publisher<geometry_msgs::msg::Pose2D>::SharedPtr publisher_;

  rclcpp::Publisher<atr_interfaces::msg::Pose2DStamped>::SharedPtr publisher_;
  size_t count_;

  




int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  Husqvarna::AutomowerSafePtr am(new Husqvarna::AutomowerSafe);
  rclcpp::spin(std::make_shared<Gpss::ATRDriver>(am));
  rclcpp::shutdown();
  return 0;
}

