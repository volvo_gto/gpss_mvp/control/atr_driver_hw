



/**
 * \author Emmanuel Dean
 *
 * \version 0.1
 * \date 13.06.2022
 *
 * \copyright Copyright 2022 Chalmers
 *
 * #### Licence
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 * #### Acknowledgment
 *  Modified from https://github.com/ros2-realtime-demo/pendulum.git
 */

#include <string>
#include <memory>
#include <chrono>
#include <functional>
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "atr_driver/am_driver_node.h"
#include "atr_driver/ATRDriver.hpp"
#include "atr_driver/ATRDriverNode.hpp"
#include <boost/shared_ptr.hpp>
#include <vector>

#include <chrono>
#include <iostream>
//#include "atr_interfaces/msg/OdomMsg.hpp"  
#include <typeinfo>
#include "atr_driver/ATRDriverNode.hpp"
#include <ctime>

using std::placeholders::_1;
using namespace std::chrono_literals;
using namespace std::chrono;
using namespace std;

atr::atr_driver::ATR_Driver_Ptr cmd;
atr::atr_driver::ATRDriverNode_Ptr adn;
Husqvarna::AutomowerSafePtr am;

/*

 Gpss::ATR_Driver::ATR_Driver(Husqvarna::AutomowerSafePtr am_t)
  : Node("ATR_driver")
  {
    std::vector<std::string> param_names = { "kp", "ki", "kd"};
    for (auto&& i : param_names){
      declare_parameter(i);
    }
    std::vector<rclcpp::Parameter> params = this->get_parameters(param_names);
    //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"pid param: "+params.at(1).as_string());

    am = am_t;
    am->setup();
    am->initAutomowerBoard(double(100), params.at(0).as_double(), params.at(1).as_double(), params.at(2).as_double());
    
    new_ref = false;
    subscription_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10, std::bind(&ATR_Driver::velocity_callback, this, _1));
    subscription_new_ref = this->create_subscription<std_msgs::msg::Bool>("new_ref", 10, std::bind(&ATR_Driver::new_ref_callback, this, _1));

    //publisher_ = this->create_publisher<geometry_msgs::msg::Pose2D>("odom", 10);
    publisher_ = this->create_publisher<msg_pkg::msg::Pose2DStamped>("odom", 10);

    publisher_ctrl_data = this->create_publisher<msg_pkg::msg::ControlSignals>("ctrl_data", 10);
    
    //auto Gpss::ATRDriver::request = std::make_shared<msg_pkg::srv::Collision::Request>();

    //Husqvarna::AutomowerSafePtr am(new Husqvarna::AutomowerSafe);
    
  }

  void Gpss::ATR_Driver::new_ref_callback(const std_msgs::msg::Bool::SharedPtr b)
  {
    
    this->new_ref = true;
    
  }
*/

namespace atr
{
namespace atr_driver
{
ATRDriverNode::ATRDriverNode(const rclcpp::NodeOptions& options) : ATRDriverNode("atr_driver", options)
{
}

ATRDriverNode::ATRDriverNode(const std::string& node_name, const rclcpp::NodeOptions& options)
  : LifecycleNode(node_name, options)
  , joint_state_topic_name_(declare_parameter<std::string>("joint_state_topic_name", "atr_joint_states"))
  , state_topic_name_(declare_parameter<std::string>("state_topic_name", "atr_states"))
  , command_topic_name_(declare_parameter<std::string>("command_topic_name", "joint_command"))
  , disturbance_topic_name_(declare_parameter<std::string>("disturbance_topic_name", "disturbance"))
  , cart_base_joint_name_(declare_parameter<std::string>("cart_base_joint_name", "cart_base_joint"))
  , state_publish_period_(
        std::chrono::microseconds{ declare_parameter<std::uint16_t>("state_publish_period_us", 1000U) })
  , enable_topic_stats_(declare_parameter<bool>("enable_topic_stats", false))
  , topic_stats_topic_name_{ declare_parameter<std::string>("topic_stats_topic_name", "driver_stats") }
  , topic_stats_publish_period_{ std::chrono::milliseconds{
        declare_parameter<std::uint16_t>("topic_stats_publish_period_ms", 1000U) } }
  , deadline_duration_{ std::chrono::milliseconds{ declare_parameter<std::uint16_t>("deadline_duration_ms", 0U) } }
  , driver_(ATRDriverConfig(
        declare_parameter<double>("driver.wheel_length", 0.48), declare_parameter<double>("driver.wheel_radius", 0.125),
        declare_parameter<double>("driver.damping_coefficient", 20.0),
        declare_parameter<double>("driver.gravity", -9.8), declare_parameter<double>("driver.max_wheel_speed", 1000.0),
        declare_parameter<double>("driver.noise_level", 1.0), std::chrono::microseconds{ state_publish_period_ }))
  , num_missed_deadlines_pub_{ 0U }
  , num_missed_deadlines_pub_state_{ 0U }
  , num_missed_deadlines_sub_{ 0U }
{
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  init_state_message();
  create_state_publisher();
  create_command_subscription();
  create_disturbance_subscription();
  create_state_timer_callback();
  ATR_Driver();
  velocity_callback();
  odometry_callback();
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
}

void ATRDriverNode::init_state_message()
{
  // state_message_.cart_position = 0.0;
  // state_message_.cart_velocity = 0.0;
  // state_message_.cart_force = 0.0;
  // state_message_.pole_angle = 0.0;
  // state_message_.pole_velocity = 0.0;
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  joint_state_message_ = std::make_shared<sensor_msgs::msg::JointState>();
  state_message_ = std::make_shared<atr_state_msgs::msg::ATRJointState>();

  // TODO: change this with parameters
  std::string atr_frame_id_ = "world";

  joint_state_message_->header.frame_id = atr_frame_id_;

  std::stringstream prefix;
  // prefix << "atr_" << atr_id_ << "_";
  prefix << "";

  // clang-format off
  joint_state_message_->name = {  prefix.str() + "base_link_axis_x_joint",
                            prefix.str() + "base_link_axis_y_joint",
                            prefix.str() + "base_link_axis_z_joint",
                            prefix.str() + "back_right_wheel_joint",
                            prefix.str() + "back_left_wheel_joint",
                            prefix.str() + "sw_arm_right_connect_1",
                            prefix.str() + "front_right_swivel_wheel_joint",
                            prefix.str() + "sw_arm_left_connect_1",
                            prefix.str() + "front_left_swivel_wheel_joint"
                        };
  // clang-format on

  joint_state_message_->position.resize(joint_state_message_->name.size());
  joint_state_message_->velocity.resize(joint_state_message_->name.size());
  joint_state_message_->effort.resize(joint_state_message_->name.size());

  std::fill(joint_state_message_->position.begin(), joint_state_message_->position.end(), 0.0);
  std::fill(joint_state_message_->velocity.begin(), joint_state_message_->velocity.end(), 0.0);
  std::fill(joint_state_message_->effort.begin(), joint_state_message_->effort.end(), 0.0);
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
}  // namespace atr_driver

void ATRDriverNode::create_state_publisher()
{
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  rclcpp::PublisherOptions sensor_publisher_options;
  sensor_publisher_options.event_callbacks.deadline_callback = [this](rclcpp::QOSDeadlineOfferedInfo&) -> void {
    num_missed_deadlines_pub_++;
  };
  rclcpp::PublisherOptions sensor_state_publisher_options;
  sensor_state_publisher_options.event_callbacks.deadline_callback = [this](rclcpp::QOSDeadlineOfferedInfo&) -> void {
    num_missed_deadlines_pub_state_++;
  };
  joint_state_pub_ = this->create_publisher<sensor_msgs::msg::JointState>(
      joint_state_topic_name_, rclcpp::QoS(10).deadline(deadline_duration_), sensor_publisher_options);

  state_pub_ = this->create_publisher<atr_state_msgs::msg::ATRJointState>(
      state_topic_name_, rclcpp::QoS(10).deadline(deadline_duration_), sensor_state_publisher_options);
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
}

void ATRDriverNode::create_command_subscription()
{
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  // Pre-allocates message in a pool
  using rclcpp::memory_strategies::allocator_memory_strategy::AllocatorMemoryStrategy;
  using rclcpp::strategies::message_pool_memory_strategy::MessagePoolMemoryStrategy;
  auto command_msg_strategy = std::make_shared<MessagePoolMemoryStrategy<atr_state_msgs::msg::ATRJointCommand, 1>>();

  rclcpp::SubscriptionOptions command_subscription_options;
  command_subscription_options.event_callbacks.deadline_callback = [this](rclcpp::QOSDeadlineRequestedInfo&) -> void {
    num_missed_deadlines_sub_++;
  };
  if (enable_topic_stats_)
  {
    command_subscription_options.topic_stats_options.state = rclcpp::TopicStatisticsState::Enable;
    command_subscription_options.topic_stats_options.publish_topic = topic_stats_topic_name_;
    command_subscription_options.topic_stats_options.publish_period = topic_stats_publish_period_;
  }
  auto on_command_received = [this](atr_state_msgs::msg::ATRJointCommand::SharedPtr msg) {
    // RCLCPP_WARN_STREAM(get_logger(),
    //                    "cmd wheel vel: " << msg->wheel_velocity.at(0) << "," << msg->wheel_velocity.at(1));
    // TODO: protect this W/R with mutex
    driver_.set_controller_wheel_vel(Eigen::Vector2d(msg->wheel_velocity.data()));
  };
  command_sub_ = this->create_subscription<atr_state_msgs::msg::ATRJointCommand>(
      command_topic_name_, rclcpp::QoS(10).deadline(deadline_duration_), on_command_received,
      command_subscription_options, command_msg_strategy);
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
}

void ATRDriverNode::create_disturbance_subscription()
{
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  auto on_disturbance_received = [this](atr_state_msgs::msg::ATRJointCommand::SharedPtr msg) {
    driver_.set_disturbance(Eigen::Vector2d(msg->wheel_velocity.data()));
  };
  disturbance_sub_ = this->create_subscription<atr_state_msgs::msg::ATRJointCommand>(
      disturbance_topic_name_, rclcpp::QoS(10), on_disturbance_received);
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
}

void ATRDriverNode::create_state_timer_callback()
{
  auto state_timer_callback = [this]() {
    // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
    driver_.update();
    const auto state = driver_.get_state();             
    // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
    // RCLCPP_INFO_STREAM(get_logger(), "State Pose: " << state.pose.transpose());
    // state_message_.cart_position = state.cart_position;
    // state_message_.cart_velocity = state.cart_velocity;
    // state_message_.cart_force = state.cart_force;
    // state_message_.pole_angle = state.pole_angle;
    // state_message_.pole_velocity = state.pole_velocity;

    joint_state_message_->position.at(0) = state.pose(0);
    joint_state_message_->position.at(1) = state.pose(1);
    joint_state_message_->position.at(2) = state.pose(2);

    joint_state_message_->velocity.at(0) = state.vel(0);
    joint_state_message_->velocity.at(1) = state.vel(1);
    joint_state_message_->velocity.at(2) = state.vel(2);

    joint_state_message_->effort.at(0) = state.wheel_vel(0);
    joint_state_message_->effort.at(1) = state.wheel_vel(1);
    joint_state_message_->header.stamp = now();
    // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
    state_message_->full_state = {
      state.pose(0), state.pose(1), state.pose(2), state.vel(0), state.vel(1), state.vel(2)
    };
    // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);

    // TODO: create another thread for the joint_state publisher. We don't need the joint_states at the same freq. as
    // the atr_state for the control
    joint_state_pub_->publish(*joint_state_message_.get());
    state_pub_->publish(*state_message_.get());
    // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  };
  state_timer_ = this->create_wall_timer(state_publish_period_, state_timer_callback);
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  // cancel immediately to prevent triggering it in this state
  state_timer_->cancel();
}

void ATRDriverNode::log_driver_state()
{
  const ATRDriver::ATRState state = driver_.get_state();
  const Eigen::Vector2d disturbance_wheel_vel = driver_.get_disturbance();
  const Eigen::Vector2d controller_wheel_vel_command = driver_.get_controller_wheel_velocity();

  RCLCPP_INFO_STREAM(get_logger(), "Cart position = " << state.pose.transpose());
  RCLCPP_INFO_STREAM(get_logger(), "Cart velocity = " << state.vel.transpose());
  RCLCPP_INFO_STREAM(get_logger(), "Controller wheel vel command = " << controller_wheel_vel_command);
  RCLCPP_INFO_STREAM(get_logger(), "Disturbance vel = " << disturbance_wheel_vel);
  RCLCPP_INFO_STREAM(get_logger(), "Publisher missed deadlines = " << num_missed_deadlines_pub_);
  RCLCPP_INFO_STREAM(get_logger(), "Subscription missed deadlines = " << num_missed_deadlines_sub_);
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_configure(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Configuring");
  // reset internal state of the driver for a clean start
  driver_.reset();
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_activate(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Activating");
  joint_state_pub_->on_activate();
  state_pub_->on_activate();
  state_timer_->reset();
  // RCLCPP_WARN_STREAM(get_logger(), __FILE__ << ":" << __LINE__);
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_deactivate(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Deactivating");
  state_timer_->cancel();
  joint_state_pub_->on_deactivate();
  state_pub_->on_deactivate();
  // log the status to introspect the result
  log_driver_state();
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_cleanup(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Cleaning up");
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}

rclcpp_lifecycle::node_interfaces::LifecycleNodeInterface::CallbackReturn
ATRDriverNode::on_shutdown(const rclcpp_lifecycle::State&)
{
  RCLCPP_INFO(get_logger(), "Shutting down");
  return LifecycleNodeInterface::CallbackReturn::SUCCESS;
}






void ATRDriverNode::ATR_Driver()
  {
    std::vector<std::string> param_names = { "kp", "ki", "kd"};
    for (auto&& i : param_names){
      declare_parameter(i);
    }
    std::vector<rclcpp::Parameter> params = this->get_parameters(param_names);
    //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"pid param: "+params.at(1).as_string());

    //am = am_t;
    am->setup();
    am->initAutomowerBoard(double(100), params.at(0).as_double(), params.at(1).as_double(), params.at(2).as_double());
    
    new_ref = false;
    //subscription_ = this->create_subscription<geometry_msgs::msg::Twist>("cmd_vel", 10, std::bind(&ATR_Driver::velocity_callback, this, _1));
    //subscription_new_ref = this->create_subscription<std_msgs::msg::Bool>("new_ref", 10, std::bind(&ATR_Driver::new_ref_callback, this, _1));

    //publisher_ = this->create_publisher<geometry_msgs::msg::Pose2D>("odom", 10);
    //publisher_ = this->create_publisher<msg_pkg::msg::Pose2DStamped>("odom", 10);

    //publisher_ctrl_data = this->create_publisher<msg_pkg::msg::ControlSignals>("ctrl_data", 10);
    
    //auto Gpss::ATRDriver::request = std::make_shared<msg_pkg::srv::Collision::Request>();

    //Husqvarna::AutomowerSafePtr am(new Husqvarna::AutomowerSafe);
    
  }

 void ATRDriverNode::new_ref_callback(const std_msgs::msg::Bool::SharedPtr b)
  {
    
    this->new_ref = true;
    
  }

  //void ATRDriverNode::velocity_callback(const geometry_msgs::msg::Twist::SharedPtr vel)
  void ATRDriverNode::velocity_callback()
  {
    auto wheeldata_timer_callback = [this](){
    am->getWheelData();
    //atr::atr_driver::ATR_Ptr cmd;
    cmd->update();
    cmd->send_wheel_cmd();

    if ((am->sensorStatus.sensor_status & 0x0004) || this->new_ref==false ) 
    {
      am->sendWheelPower(0.0, 0.0);
      RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Collision - Node - stop wheels");

      this->new_ref=false;
      
      
    }
    else
    {
      //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"No Collision - Node- velocity callback");
      am->wanted_lv = cmd->wheel_left;
      am->wanted_rv = cmd->wheel_right;

      if ((am->wanted_rv < 0.025) && (am->wanted_lv < 0.025) && (am->wanted_rv > -0.025) && (am->wanted_lv > -0.025)) {
        am->wanted_lv = 0;
        am->wanted_rv = 0;
      }


      am->regulateVelocity();
    }
    

    // publish controller signal
    /*
    this->message_ctrl_data.power_l = am->power_l;
    this->message_ctrl_data.power_r = am->power_r;
    this->message_ctrl_data.wanted_lv = am->wanted_lv;
    this->message_ctrl_data.wanted_rv = am->wanted_rv;
    this->message_ctrl_data.current_lv = am->current_lv;
    this->message_ctrl_data.current_rv = am->current_rv;
    this->message_ctrl_data.ref_lin_vel = this->lin_vel;
    this->message_ctrl_data.ref_ang_vel = this->ang_vel;

    //publisher_ctrl_data->publish(message_ctrl_data);
    */
   
    };
    adn-> wheel_data_timer_ = this->create_wall_timer(6ms, wheeldata_timer_callback);
    adn-> wheel_data_timer_->cancel();

    //RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
  }

  void ATRDriverNode::odometry_callback()
  {
    
    auto encoder_data_timer_callback = [this](){
    am->getEncoderData();
    am->updateOdometry(double(100));

    adn->state_message_->full_state = {am-> xpos, am-> ypos, am-> yaw, am-> vx, am-> vy, am-> vYaw};
    adn->state_pub_->publish(*(adn->state_message_).get());
    };
    
    auto sensor_status_timer_callback = [this](){
    am->getSensorStatus();
    };
    /*
    this->message.pose.x = am->xdist;
    this->message.pose.y = am->ydist;
    this->message.pose.theta = am->delta_yaw;
    this->message.timestamp = (double) duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
    publisher_->publish(message);
    */

    adn-> encoder_data_timer_ = this->create_wall_timer(12ms, encoder_data_timer_callback);
    adn-> encoder_data_timer_->cancel();

    adn-> sensor_status_timer_ = this->create_wall_timer(24ms, sensor_status_timer_callback);
    adn-> sensor_status_timer_->cancel();

  }




}  // namespace atr_driver
}  // namespace atr


/*
  void Gpss::ATR_Driver::velocity_callback(const geometry_msgs::msg::Twist::SharedPtr vel)
  {
    auto wheeldata_timer_callback = [this](){
    am->getWheelData();
    //atr::atr_driver::ATR_Ptr cmd;
    cmd->update();
    cmd->send_wheel_cmd();

    if ((am->sensorStatus.sensor_status & 0x0004) || this->new_ref==false ) 
    {
      am->sendWheelPower(0.0, 0.0);
      RCLCPP_INFO(rclcpp::get_logger("my_logger"),"Collision - Node - stop wheels");

      this->new_ref=false;
      
      
    }
    else
    {
      //RCLCPP_INFO(rclcpp::get_logger("my_logger"),"No Collision - Node- velocity callback");
      am->wanted_lv = cmd->wheel_left;
      am->wanted_rv = cmd->wheel_right;

      if ((am->wanted_rv < 0.025) && (am->wanted_lv < 0.025) && (am->wanted_rv > -0.025) && (am->wanted_lv > -0.025)) {
        am->wanted_lv = 0;
        am->wanted_rv = 0;
      }


      am->regulateVelocity();
    }
    

    // publish controller signal
    this->message_ctrl_data.power_l = am->power_l;
    this->message_ctrl_data.power_r = am->power_r;
    this->message_ctrl_data.wanted_lv = am->wanted_lv;
    this->message_ctrl_data.wanted_rv = am->wanted_rv;
    this->message_ctrl_data.current_lv = am->current_lv;
    this->message_ctrl_data.current_rv = am->current_rv;
    this->message_ctrl_data.ref_lin_vel = this->lin_vel;
    this->message_ctrl_data.ref_ang_vel = this->ang_vel;

    publisher_ctrl_data->publish(message_ctrl_data);

    };
    adn-> wheel_data_timer_ = this->create_wall_timer(6ms, wheeldata_timer_callback);
    adn-> wheel_data_timer_->cancel();

    //RCLCPP_INFO(this->get_logger(), "I heard: '%s'", msg->data.c_str());
  }
  rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscription_;
  rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr subscription_new_ref;
  //rclcpp::Client<msg_pkg::srv::Collision>::SharedPtr client_;


  void Gpss::ATR_Driver::odometry_callback()
  {
    
    auto encoder_data_timer_callback = [this](){
    am->getEncoderData();
    am->updateOdometry(double(100));

    adn->state_message_->full_state = {am-> xpos, am-> ypos, am-> yaw, am-> vx, am-> vy, am-> vYaw};
    adn->state_pub_->publish(*(adn->state_message_).get());
    };
    
    auto sensor_status_timer_callback = [this](){
    am->getSensorStatus();
    };
    
    //this->message.pose.x = am->xdist;
    //this->message.pose.y = am->ydist;
    //this->message.pose.theta = am->delta_yaw;
    //this->message.timestamp = (double) duration_cast< milliseconds >(system_clock::now().time_since_epoch()).count();
    //publisher_->publish(message);
    

    adn-> encoder_data_timer_ = this->create_wall_timer(12ms, encoder_data_timer_callback);
    adn-> encoder_data_timer_->cancel();

    adn-> sensor_status_timer_ = this->create_wall_timer(24ms, sensor_status_timer_callback);
    adn-> sensor_status_timer_->cancel();

  }
  rclcpp::TimerBase::SharedPtr timer_pub;
  //rclcpp::Publisher<geometry_msgs::msg::Pose2D>::SharedPtr publisher_;

  rclcpp::Publisher<msg_pkg::msg::Pose2DStamped>::SharedPtr publisher_;
*/
#include "rclcpp_components/register_node_macro.hpp"

// Register the component with class_loader.
// This acts as a sort of entry point,
// allowing the component to be discoverable when its library
// is being loaded into a running process.
RCLCPP_COMPONENTS_REGISTER_NODE(atr::atr_driver::ATRDriverNode)
