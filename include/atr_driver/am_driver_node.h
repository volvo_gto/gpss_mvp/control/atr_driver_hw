/*
 * Copyright (c) 2014 - Husqvarna AB, part of HusqvarnaGroup
 * Author: Stefan Grufman
 *  	   Kent Askenmalm
 *
 */

#ifndef AUTOMOWER_SAFE_H
#define AUTOMOWER_SAFE_H

#include <rclcpp/rclcpp.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <geometry_msgs/msg/pose2_d.hpp>
#include <geometry_msgs/msg/point.hpp>
#include <geometry_msgs/msg/point_stamped.hpp>
#include <nav_msgs/msg/odometry.hpp>
#include <std_msgs/msg/bool.hpp>
//#include <tf/transform_broadcaster.h> REMOVED
#include <husqvarna_msgs/msg/loop.hpp>
#include <husqvarna_msgs/msg/sensor_status.hpp>
#include <husqvarna_msgs/msg/battery_status.hpp>
#include <husqvarna_msgs/msg/wheel_power.hpp>
#include <husqvarna_msgs/msg/control_signals.hpp>
#include <std_msgs/msg/u_int16.hpp>
#include <sensor_msgs/msg/imu.hpp>
#include <husqvarna_msgs/msg/wheel_encoder.hpp>
#include <husqvarna_msgs/msg/wheel_current.hpp>
//#include <am_driver_ros2/TifCmd.h> Removed (srv not yet included)
#include <husqvarna_msgs/msg/motor_feedback.hpp>
#include <husqvarna_msgs/msg/motor_feedback_diff_drive.hpp>
#include <sensor_msgs/msg/nav_sat_fix.hpp>
#include <rclcpp/parameter_client.hpp>
#include <sys/select.h>
//#include "atr_interfaces/msg/pose2_d_stamped.hpp"
#include "husqvarna_msgs/msg/pose2_d_stamped.hpp"
#include "atr_driver/ATRDriver.hpp"
#include <vector>

// Removed HQ Decision making
//#include <hq_decision_making/hq_FSM.h>
//#include <hq_decision_making/hq_ROSTask.h>
//#include <hq_decision_making/hq_DecisionMaking.h>




#ifdef __cplusplus
extern "C"
{
#endif

    // To get rid of loads of compiler warnings...
    #pragma GCC diagnostic ignored "-Wwrite-strings"

    #include "hcp_types.h"
    #include "hcp_runtime.h"
    #include "hcp_string.h"
    #include "hcp_library.h"

    #include "amg3.h"

    #pragma GCC diagnostic pop

    typedef struct tCodec
    {
        char* path;
        hcp_szStr name;
        hcp_tCodecLibrary* lib;
    } tCodec;

    typedef struct
    {
        hcp_tVector header;
        hcp_tCodec fixed[HCP_MAXSIZE_CODECS];
    } tCodecSet;

#ifdef __cplusplus
}
#endif


#define	AM_STATE_UNDEFINED     0x0
#define	AM_STATE_IDLE          0x1
#define	AM_STATE_INIT          0x2
#define	AM_STATE_MANUAL        0x3
#define	AM_STATE_RANDOM        0x4
#define	AM_STATE_PARK          0x5



namespace Husqvarna
{

class PidRegulator
{
public:
    void Init(double in_p, double in_i, double in_d)
    {
        p = in_p;
        i = in_i;
        d = in_d;

        Restart();
    }

    void Restart()
    {
        pErr = 0.0;
        iErr = 0.0;
        dErr = 0.0;
    }

    double Update(double currentSignal, double wantedSignal)
    {
        double errOld = pErr;
        double err = (wantedSignal - currentSignal);

        pErr = err;
        iErr = iErr + errOld;
        dErr = err - errOld;

        double out = p*pErr + i*iErr + d*dErr;

        //std::cout << "out: " << out << " pErr:" << pErr << " iErr:" << iErr << " dErr:" << dErr << std::endl;

        return out;
    }

private:
    double pErr;
    double iErr;
    double dErr;

    // Gains
    double p;
    double i;
    double d;

};






class AutomowerSafe
{
public:
    //const ros::NodeHandle& nodeh, decision_making::RosEventQueue* eq);
    AutomowerSafe();
    ~AutomowerSafe();

    bool setup();
    bool update(); //rclcpp::Duration dt);
    
    //decision_making::RosEventQueue* eventQueue;

    bool m_regulatingActive;
    


	void pauseMower();
	void startMower();
	void setParkMode();
	void setAutoMode();
	void cutDiscHandling();
	void cutDiscOff();
        void stopWheels();
	void loopDetectionHandling();
	void cuttingHeightHandling();
	
    void newControlMainState(int aNewState);
    
    int GetUpdateRate();
    double wanted_lv, wanted_rv;
    double AUTMOWER_WHEEL_BASE_WIDTH;
    void regulateVelocity();
    bool initAutomowerBoard(double regulatorFreq, double kp, double ki, double kd);
    void updateOdometry(double updateFreq);
    bool getEncoderData();
    bool getWheelData();

    bool getSensorStatus();
    husqvarna_msgs::msg::SensorStatus sensorStatus;
    double xpos, ypos, yaw;
    double xdist, ydist, delta_yaw;
    double vx, vy,vYaw;
    void sendWheelPower(double power_left,
                        double power_right);
    int16_t power_l, power_r;
    double current_lv, current_rv;
private:
    void velocityCallback(const geometry_msgs::msg::Twist::ConstPtr& vel);
    void powerCallback(const husqvarna_msgs::msg::WheelPower::ConstPtr& power);
    void modeCallback(const std_msgs::msg::UInt16::ConstPtr& msg);
    
    std::string resultToString(hcp_tResult result);
    
    bool sendMessage(const char* msg, int len, hcp_tResult& result);
    void imuResetCallback(const geometry_msgs::msg::Pose::ConstPtr& msg);
    void setPower();
    
    
    bool getSensorData();
    bool getPitchAndRoll();
    bool getGPSData();
    bool getStateData();
    
    bool getLoopData();
    bool getBatteryData();

     

    bool isTimeOut(); //rclcpp::Duration elapsedTime, double frequency);

    // bool executeTifCommand(am_driver_safe::TifCmd::Request& req,
                                      // am_driver_safe::TifCmd::Response& res); Removed

    double regulatePid(double current_vel, double wanted_vel);
    bool doSerialComTest();
    void handleCollisionInjections(); //rclcpp::Duration dt);

    std::string loadJsonModel(std::string fileName);

    // ROS data
    //ros::NodeHandle nh;

    //ros::ServiceServer tifCommandService;
    //ros::Subscriber velocity_sub;
    //ros::Subscriber power_sub;
    //ros::Subscriber cmd_sub;
    //ros::Subscriber imu_sub;
    //ros::Subscriber imu_euler_sub;

    //ros::Publisher pose_pub;
    //ros::Publisher odom_pub;
    //ros::Publisher euler_pub;


    //ros::Publisher loop_pub;
    //ros::Publisher sensorStatus_pub;
    //ros::Publisher batStatus_pub;

    //ros::Publisher encoder_pub;
    //ros::Publisher current_pub;
    //ros::Publisher wheelPower_pub;
    //ros::Publisher motorFeedbackDiffDrive_pub;
    
    //ros::Publisher navSatFix_pub;
    //tf::TransformBroadcaster br;

    
    double updateRate;

    double encoderSensorFreq;
    double wheelSensorFreq;
    //double regulatorFreq;
    double setPowerFreq;
    double stateCheckFreq;
    double loopSensorFreq;
    double batteryCheckFreq;
    double GPSCheckFreq;
    double pitchRollFreq;
    double sensorStatusCheckFreq;

    //rclcpp::Duration timeSinceWheelSensor;
    //rclcpp::Duration timeSinceEncoderSensor;
    //rclcpp::Duration timeSinceRegulator;
    //rclcpp::Duration timeSinceState;
    //rclcpp::Duration timeSinceLoop;
    //rclcpp::Duration timeSincePitchRoll;
    //rclcpp::Duration timeSincebattery;
    //rclcpp::Duration timeSinceGPS;
    //rclcpp::Duration timeSinceStatus;
    //rclcpp::Duration timeSinceCollision;
    

    // Control data
    double lin_vel;
    double ang_vel;

    geometry_msgs::msg::PoseStamped robot_pose;

    
    double wanted_power_left, wanted_power_right;
    
    double last_yaw;
    

    rclcpp::Time lastEncoderSampeTime;
    bool automowerInterfaceInited;
    int leftPulses;
    int rightPulses;
    int lastLeftPulses;
    int lastRightPulses;
    int leftTicks;
    int rightTicks;

    // Parameters
    std::string pSerialPort;
    std::string robot_name;
    bool serialComTest;
    bool serialLog;

    // Mower parameters (treated as const, that is why capital letters...sorry)
    double WHEEL_DIAMETER;
    int WHEEL_PULSES_PER_TURN;
    double WHEEL_METER_PER_TICK;
    

    // Serial port handle
    int serialFd;

    // Serial port state
    int serialPortState;

    // Automower status
    husqvarna_msgs::msg::Loop loop;
    
    unsigned short requestedState;
    
    
    husqvarna_msgs::msg::WheelEncoder encoder;
    husqvarna_msgs::msg::WheelCurrent wheelCurrent;
    husqvarna_msgs::msg::WheelPower wheelPower;
    husqvarna_msgs::msg::MotorFeedbackDiffDrive motorFeedbackDiffDrive;

    int publishTf;
    int velocityRegulator;
    bool regulateBySpeed;

    bool newSound;
    char soundCmd[100];

    int collisionState;

    // Cutting disc
    bool cuttingDiscOn;
    bool lastCuttingDiscOn;
    unsigned char cuttingHeight;
    unsigned char lastCuttingHeight;

    uint8_t actionResponse;

	bool requestedLoopOn;

    // For HCP Library
    hcp_tHost hcpHost;
    hcp_tState* hcpState;
    tCodecSet hcpCodecs;
    std::string jsonFile;
    hcp_Size_t codecId;
    hcp_Int modelId;

    PidRegulator leftWheelPid;
    PidRegulator rightWheelPid;

    rclcpp::Time nextAutomowerInitTime;
    double lastRateCheckTime;
    int lastComtestWheelMotorPower;
    double startTime;

    bool startWithoutLoop;
    bool publishEuler;

    bool printCharge;
    husqvarna_msgs::msg::BatteryStatus batteryStatus;

    // UserStop
    bool userStop;
    
    // Accelerometer
    bool   m_PitchAndRollFromAccelerometer;
    double m_pitch;
    double m_roll;
    
  
    
	// GPS
	sensor_msgs::msg::NavSatFix      m_navSatFix_msg;
	bool						m_publishGPS;

    //tf::Transform transform;

};

typedef boost::shared_ptr<AutomowerSafe> AutomowerSafePtr;
}
/*
namespace Gpss
{
class ATRDriver : public rclcpp::Node
{
    public:
        ATRDriver(Husqvarna::AutomowerSafePtr am_t);
        

     	rclcpp::Publisher<msg_pkg::msg::Pose2DStamped>::SharedPtr publisher_ = nullptr;
     	rclcpp::Publisher<msg_pkg::msg::ControlSignals>::SharedPtr publisher_ctrl_data = nullptr;
     	rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr subscription_ = nullptr;
     	rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr subscription_new_ref = nullptr;
     	rclcpp::TimerBase::SharedPtr timer_pub = nullptr;
     	//rclcpp::Client<msg_pkg::srv::Collision>::SharedPtr client_ = nullptr;
     	
        void velocity_callback(const geometry_msgs::msg::Twist::SharedPtr vel);
        void odometry_callback();
        void new_ref_callback(const std_msgs::msg::Bool::SharedPtr b);
        
        float var = 0;
      	msg_pkg::msg::Pose2DStamped message;
      	msg_pkg::msg::ControlSignals message_ctrl_data;
      	//std::shared_ptr<msg_pkg::srv::Collision::Request> request;
      	double lin_vel;
      	double ang_vel;
      	double wanted_lv;
      	double wanted_rv;
      	bool new_ref;
      	
      	Husqvarna::AutomowerSafePtr am;
    protected:
    private:
};
}
*/
#endif
